function studentController($scope) {
    $scope.reset = function () {
        $scope.firstName = "";
        $scope.lastName = "";
        $scope.email = "";
    }
    $scope.reset();
}